import pirc
import interface
import curses

# Host and port
HOST = "irc.patwic.com"
PORT = 6667

# IRC server password
PASSWORD = "foobar"

# Create IRC server connection object
irc = pirc.IRC(HOST, PORT, PASSWORD)

# User data
NICK = "evilbunny"
REALNAME = "The Great Leader"

# Connect to server and register user
irc.connect(NICK, REALNAME)

# Channel
CHAN = "#patwic"

# MOTD
motd = "Santa is definitely a communist.\r\n"

# Join channel
irc.send("JOIN " + CHAN)


def filter_msg(msg):
	""" procces IRC message and return it more pretty """
	name_marker = msg.find("!")
	sender = msg[1: name_marker]
	msg_marker = msg.rfind(':') + 1
	message = "<" + sender + "> : " + msg[msg_marker : ]
	return message
	

def main(stdscreen):

	muted = [""]

	ui = interface.TextUI(stdscreen)

	have_written = False

	irc.send("PRIVMSG #patwic :MOTD: ")
	irc.send(("PRIVMSG #patwic :" + motd) * 5)

	while True:
		msg = irc.read()
		if msg and (msg[1 : msg.find("!")] not in muted):
			ui.output(filter_msg(msg))
#			if have_written:
#				irc.send("PRIVMSG #patwic :" + filter_msg(msg))



		my_msg = ui.input()

		if my_msg:
			if my_msg.startswith("/"):

				if my_msg.startswith("/exit"):
					irc.send("PRIVMSG " + CHAN + my_msg[my_msg.find(" ") : ])
					irc.send("QUIT")
					return
	
				elif my_msg.startswith("/msg"):
					my_msg = my_msg[my_msg.find(" ") + 1 : ]
					recipient = my_msg[ : my_msg.find(" ")]
					my_msg = my_msg[my_msg.find(" ") + 1 : ]
					irc.send("PRIVMSG " + recipient + " : " + my_msg)
					ui.output("<" + NICK + "> : " + my_msg)

				elif my_msg.startswith("/users"):
					irc.send("NAMES " + CHAN)

				elif my_msg.startswith("/motd"):
					irc.send("PRIVMSG #patwic :MOTD: ")
					irc.send(("PRIVMSG #patwic :" + motd) * 5)

				elif my_msg.startswith("/mute"):
					muted.append(my_msg[my_msg.find(" ") + 1 :])

				else:
					ui.output("command :" + my_msg + " not found.")


			else :
				irc.send("PRIVMSG #patwic :" + my_msg)
				ui.output("<" + NICK + "> : " + my_msg)
				have_written = True
	return


curses.wrapper(main)
